# Final MisPalabras

Práctica final del curso 2021/22

## Datos
* Nombre: Veselin Todorov Hristov
* Titulación: Grado en Ingeniería Telemática

* Requerimientos:
    - beautifulsoup4
    - cloudscraper
    - request
    
=======
* Despliegue(url): http://veselin.pythonanywhere.com/
* Debido a que pythonanywhere solo permite acceder a páginas que se encuentran en su whitelist(https://www.pythonanywhere.com/whitelist/), algunas funcionalidades como añadir la definición de la RAE y aportar urls a las palabras no funcionan correctamente.
* Páginas mas conocidas de la whitelist:
    - .youtube.com
    - .pinterest.com
    - open.spotify.com
    - .facebook.com

* En los videos explicativos se puede ver bastante bien la funcionalidad de la web :)
* Videos explicativos(url): 
    - Parte obligatoria: https://www.youtube.com/watch?v=8JDdf9FfB48
    - Parte opcional: https://www.youtube.com/watch?v=aXCF8p4zTKo
 
## Cuenta admin site
* admin/admin

## Cuentas registradas
* pepe/pepepass
* carlos/carlospass


## Lista partes opcionales
* Inclusión de un favicon del sitio
