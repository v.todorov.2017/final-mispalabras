#!/usr/bin/python3
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib.request
import json

datos = ['', '']

class Handler(ContentHandler):
    def __init__ (self):
        self.isExtract = False
        self.content = "";

    def startElement (self, name, attrs):
        if name == 'extract':
            self.isExtract = True
        if name == 'page':
            datos[1]= attrs.get('pageid')

    def endElement (self, name):
        if name == 'extract':
            self.isExtract = False
            datos[0] = self.content
            self.content = ""

    def characters (self, chars):
        if self.isExtract:
            self.content = self.content + chars


def wiki_parser(word):

    # Primero hago un parser XML para obtener 400 caracteres de texto
    url_texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+word+"&prop=extracts&exintro&explaintext"
    xmlStream = urllib.request.urlopen(url_texto)

    Parser = make_parser()
    Parser.setContentHandler(Handler())
    Parser.parse(xmlStream)
    texto = datos[0]


    # Ahora hago un parser JSON para obtener la imagen
    url_imagen = "https://es.wikipedia.org/w/api.php?action=query&titles="+word+"&prop=pageimages&format=json&pithumbsize=100"
    response = urllib.request.urlopen(url_imagen)
    jsonStream = json.loads(response.read())

    return texto[0:399], jsonStream['query']["pages"][datos[1]]["thumbnail"]["source"], 2



def info_wiki(word):
    (texto, imagen, copyright) = wiki_parser(word)
    html = '<div class="wikipedia">\
    <p>Artı́culo Wikipedia: '+texto+'</p>\
    <p><img src="'+imagen+'"></p>\
    <p><a href="'+word+'">Artı́culo original</a>.</p>\
    </div>'

    return html