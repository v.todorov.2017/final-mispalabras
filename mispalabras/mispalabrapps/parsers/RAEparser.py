import urllib.request
from bs4 import BeautifulSoup


def DescargaRae(name):
    url = 'https://dle.rae.es/' + name
    # Para acceder a la página de la RAE necesitamos el siguiente código proporcinado en el punto 8.8 de la práctica
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        htmlStream = response.read().decode('utf8')

    # Con el siguiente módulo extraemos datos e información de la web
    soup = BeautifulSoup(htmlStream, 'html.parser')
    # Imprimimos el título extraido
    print("Title:", soup.title.string)
    # Busca la información que su propiedad es "og:description"
    ogDescription = soup.find('meta', property='og:description')
    # Mostramos la definición de la palabra que esta contenido en og:description
    if ogDescription:
        significado = "Definición RAE:" + ogDescription['content']
    # Busca la imagen que su propiedad es "og:image"
    ogImage = soup.find('meta', property='og:image')
    # Imprimimos la imagen de la palabra que esta contenida en og:image
    if ogImage:
        print("Image (og:image):", ogImage['content'])
    return significado