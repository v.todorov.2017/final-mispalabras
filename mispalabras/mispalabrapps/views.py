import random
import urllib.request
import json
from django.contrib.auth import logout, authenticate, login
from .parsers.WIKIparser import info_wiki
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.utils import timezone
# Create your views here.
from bs4 import BeautifulSoup
import cloudscraper
from urllib.request import urlopen
import xml.etree.ElementTree as ET
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .forms import PalabraForm, UserForm, ComentarioForm
from .models import Usuario, Palabra, Comentario, Urls, Votos


def total_palabras():
    list_palabras = []
    palabras = Palabra.objects.all()
    for palabra in palabras:
        list_palabras.append(palabra)

    list_palabras.reverse()
    return list_palabras


def palabra_aleatoria(lista):
    if len(lista) != 0:
        num_aleatorio = random.randint(0, len(lista) - 1)
        palabra_rand = lista[num_aleatorio]
    else:
        palabra_rand = None
    return palabra_rand


def total_comentarios():
    list_coments = []
    coments = Comentario.objects.all()
    for coment in coments:
        list_coments.append(coment)
    list_coments.reverse()
    list_coments = list_coments[:5]
    return list_coments


def total_enlaces():
    list_enlaces = []
    enlaces = Urls.objects.all()
    for enlace in enlaces:
        list_enlaces.append(enlace)
    list_enlaces.reverse()
    return list_enlaces


def logout_view(request):
    logout(request)
    return redirect("/")


def buscaRae(word):
    url = 'https://dle.rae.es/' + word
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        htmlStream = response.read().decode('utf8')

    soup = BeautifulSoup(htmlStream, 'html.parser')
    print("Title:", soup.title.string)
    ogDescription = soup.find('meta', property='og:description')
    if ogDescription:
        significado = word + ": " + ogDescription['content']
        return significado
    else:
        return None

def buscaEmbebida(url):
    url = url
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        htmlStream = response.read().decode('utf8')

    soup = BeautifulSoup(htmlStream, 'html.parser')
    print("Title:", soup.title.string)
    ogDescription = soup.find('meta', property='og:description')
    ogImage = soup.find('meta', property='og:image')
    title = soup.title.string
    if ogDescription:
        descripcion = "Descripción: " + ogDescription['content']
    else:
        descripcion = "No se encuentra descripción para esta url"
    if ogImage:
        imagen = ogImage['content']
    else:
        imagen = ""
    return title, descripcion, imagen

def buscaFlickr(palabra):
    try:
        url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + palabra
        scraper = cloudscraper.create_scraper()
        soup = BeautifulSoup(scraper.get(url).text, 'html.parser')
        urlimagen = soup.find('feed').findAll('entry')[0]
        for a in urlimagen.findAll('link', {'rel': 'enclosure'}, href=True):
            link = a['href']
    except:
        link = "http://apimeme.com/meme?meme=" + "Doge" + "&top=PUES+ESTA+PALABRA...&bottom=NO+TIENE+IMAGEN+EN+FLICKR"
    return link

@csrf_exempt
def index(request):
    if request.GET.get("format") == "xml":
        template = loader.get_template('mispalabrapps/xml.html')
        context = {'entradas': Palabra.objects.all()}
        return HttpResponse(template.render(context, request))
    if request.GET.get("format")=="json":
        queryset = Palabra.objects.filter().values()
        return JsonResponse({"Contenidos": list(queryset)})

    if request.method == "POST":
        url = request.POST['palabra']
        return redirect("/" + url)
    else:
        context = {'list_palabras': total_palabras()[:10],
                   'numero_palabras': len(total_palabras()),
                   'palabra_aleatoria': palabra_aleatoria(total_palabras())}
        return render(request, 'mispalabrapps/inicio.html', context)


def palabra_page(request, llave):
    if request.method == "POST":
        if request.POST['action'] == 'Añadir Palabra':
            word = Palabra(usuario=request.user, palabra=llave, imagen=wikiImagen(llave), wikidef=wikiDefinicion(llave),
                            raedef="", flickr="", meme="", fecha=timezone.now())
            word.save()
            newWord = Palabra.objects.get(palabra=llave)

            context = {'list_palabras': total_palabras()[:10],
                       'numero_palabras': len(total_palabras()),
                       'palabra':  Palabra.objects.get(palabra=llave),
                       'palabra_aleatoria': palabra_aleatoria(total_palabras())}
            return render(request, 'mispalabrapps/palabra.html', context)

        if request.POST['action'] == 'Comentar':
            contenido = request.POST['contenido']
            word = Palabra.objects.get(palabra=llave)
            coment = Comentario(usuario=request.user, palabra=word, contenido=contenido, fecha=timezone.now())
            coment.save()

        if request.POST['action'] == 'Aportar':
            url = request.POST['url']
            word = Palabra.objects.get(palabra=llave)
            [title, desc, img] = buscaEmbebida(url)
            enlace = Urls(usuario=request.user, palabra=word, url=url, embedesc=desc, embeimg=img, embetitle=title,
                          fecha=timezone.now())
            enlace.save()

        if request.POST['action'] == 'Flickr':
            word = Palabra.objects.get(palabra=llave)
            word.flickr = buscaFlickr(word.palabra)
            word.save()

        if request.POST['action'] == 'DRAE':
            word = Palabra.objects.get(palabra=llave)
            word.raedef = buscaRae(word.palabra)
            word.save()

        if request.POST['action'] == 'Meme':
            word = Palabra.objects.get(palabra=llave)
            opcion = request.POST['opcionmeme']
            if opcion == "1":
                meme = "Advice Yoda"
            elif opcion == "2":
                meme = "Table Flip Guy"
            elif opcion == "3":
                meme = "Gollum"
            else:
                meme = "Computer Guy"
            texto = request.POST['texto']
            word.meme = "http://apimeme.com/meme?meme=" + meme + "&top=" + word.palabra + "&bottom=" + texto
            word.save()

        if request.POST['action'] == 'Like':
            word = Palabra.objects.get(palabra=llave)
            try:
                voto = Votos.objects.get(palabra=word, usuario=request.user)
                voto.valor = 1
                voto.fecha = timezone.now()
            except voto.DoesNotExist:
                voto = Votos(palabra=llave, valor=1, usuario=request.user, fecha=timezone.now())
                voto.save()

        if request.POST['action'] == 'Dislike':
            try:
                voto = Votos.objects.get(palabra=word, usuario=request.user)
                voto.valor = -1
                voto.fecha = timezone.now()
            except voto.DoesNotExist:
                voto = Votos(palabra=llave, valor=-1, usuario=request.user, fecha=timezone.now())
                voto.save()
    try:
        word = Palabra.objects.get(palabra=llave)
        context = {'list_palabras': total_palabras()[:10],
                   'list_coments': Comentario.objects.filter(palabra=word),
                   'list_votos':Votos.objects.filter(palabra=word),
                   'numero_palabras': len(total_palabras()),
                   'list_urls': Urls.objects.filter(palabra=word),
                   'palabra': word,
                   'palabra_aleatoria': palabra_aleatoria(total_palabras())}
        return render(request, 'mispalabrapps/palabra.html', context)
    except Palabra.DoesNotExist:
        word = llave
        context = {'list_palabras': total_palabras(),
                   'wikidef': wikiDefinicion(llave),
                   'imagen': wikiImagen(llave),
                   'numero_palabras': len(total_palabras()),
                   'palabranueva': word,
                   'palabra_aleatoria': palabra_aleatoria(total_palabras())}
        return render(request, 'mispalabrapps/palabranueva.html', context)



def registro(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            u = Usuario(user=user)
            u.save()
        return redirect("/login")
    else:
        return render(request, 'mispalabrapps/registro.html')



def wikiImagen(palabra):
    url = "https://es.wikipedia.org/w/api.php?action=query&titles=" + palabra \
          + "&prop=pageimages&format=json&pithumbsize=200"
    query = str(urlopen(url).read().decode('utf-8'))
    objeto = json.loads(query)
    url_media = objeto['query']['pages']
    id = str(url_media).split("'")[1]
    try:
        url_imagen = url_media[id]['thumbnail']['source']
    except KeyError:
        url_imagen = "http://apimeme.com/meme?meme=" + "Doge" + "&top=PUES+ESTA+PALABRA...&bottom=NO+TIENE+IMAGEN+EN+WIKIPEDIA"
    return url_imagen

def wikiDefinicion(palabra):
    try:
        url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + palabra \
              + "&prop=extracts&exintro&explaintext"
        doc = ET.parse(urlopen(url))
        text = doc.find('query/pages/page/extract').text
        if (text == None):
            text = "La Wikipedia española no contiene esta palabra"
    except:
        text = "La Wikipedia española no contiene esta palabra"
    return text

def help(request):
    context = {'list_palabras': total_palabras()[:10],
               'numero_palabras': len(total_palabras()),
               'palabra_aleatoria': palabra_aleatoria(total_palabras())}
    return render(request, 'mispalabrapps/help.html', context)


def usuario(request):
    addWords= Palabra.objects.filter(usuario=request.user.username)
    addComents = Comentario.objects.filter(usuario=request.user.username)
    context = {'list_palabras_user': addWords,
               'list_coments_user': addComents,
               'list_palabras': total_palabras()[:10],
               'numero_palabras': len(total_palabras()),
               'list_urls': Urls.objects.filter(usuario=request.user.username),
               'palabra_aleatoria': palabra_aleatoria(total_palabras())}
    return render(request, 'mispalabrapps/usuario.html', context)
