from django.db import models
from django.contrib.auth.models import User


class Usuario(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Palabra(models.Model):
    usuario = models.CharField(max_length=64)
    palabra = models.CharField(max_length=64)
    imagen = models.URLField(default="")
    wikidef = models.TextField(default="")
    raedef = models.TextField(default="")
    flickr = models.URLField(default="")
    meme = models.URLField(default="")
    fecha = models.DateTimeField()


class Urls(models.Model):
    usuario = models.CharField(max_length=64)
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    url = models.URLField(default="")
    embetitle = models.TextField(default="")
    embedesc = models.TextField(default="")
    embeimg = models.URLField(default="")
    fecha = models.DateTimeField()


class Comentario(models.Model):
    usuario = models.CharField(max_length=64)
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    contenido = models.TextField()
    fecha = models.DateTimeField()


class Votos(models.Model):
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.IntegerField(default=0)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField()
