from django import forms
from django.contrib.auth.models import User
from .models import Palabra, Comentario


class PalabraForm(forms.ModelForm):
    class Meta:
        model = Palabra
        fields = ['palabra']


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password']


class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['contenido']
