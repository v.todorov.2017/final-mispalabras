from django.apps import AppConfig


class MispalabrappsConfig(AppConfig):
    name = 'mispalabrapps'
