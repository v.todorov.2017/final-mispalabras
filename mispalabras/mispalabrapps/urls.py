from django.urls import path

from . import views

urlpatterns = [
    path('login', views.login, name="login"),
    path('logout', views.logout_view, name="logout"),
    path('registro', views.registro, name="registro"),
    path('usuario', views.usuario, name="usuario"),
    path('help', views.help, name="help"),
    path('<str:llave>', views.palabra_page, name="palabra_page"),
    path('', views.index, name="inicio"),
]

